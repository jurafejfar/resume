The Markdown Resume
===================

Taken from https://github.com/mszep/pandoc_resume

### Instructions

```bash
git clone https://gitlab.com/jurafejfar/resume.git
cd pandoc_resume
vim markdown/resume.md   # insert your own resume info
```

#### Local

Make everything

```bash
make
```

Make specifics

```bash
make pdf
make html
```

#### Dockerized

Make everything

```bash
docker-compose up -d
```
