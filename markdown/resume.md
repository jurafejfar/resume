Jiří Fejfar
===========

----

> Date of birth: 13th May 1983\
> Slezská 937, Rožnov pod Radhoštěm, Czech republic\
> +420 605 984 592 • <jurafejfar@gmail.com>\
> [jurafejfar.gitlab.io/resume](https://jurafejfar.gitlab.io/resume)
> • [gitlab.com/jurafejfar](https://gitlab.com/jurafejfar)
> • [linkedin.com/in/jurafejfar](https://gitlab.com/jurafejfar)

----

Work experience
---------------

### Data Scientist, DevOps Engineer
**Oct 2013 – Current •
[Forest Management Institute](http://nil.uhul.cz/) •
Kroměříž, CZ**

Design and implementation of **[National Forest Inventory](http://nil.uhul.cz/) (NFI) estimation tools**:

* [nFIESTA](https://gitlab.com/nfiesta/nfiesta_pg/) -- open-source DB application for forest parameters estimation (current NFI estimation tool). Also involved in formulation of [methods](https://gitlab.com/nfiesta/nfiesta_pg/-/wikis/myuploads/nfiesta_methods/nfiesta_methods.pdf) (implementation feedback). Mainly SQL.
* Prototype of selected estimators implemented in Python (Numpy + Scipy, Matplotlib, jupyter notebooks).

Formulation and adoption of **DB development methodology**:

* [Evolutionary database design](https://en.wikipedia.org/wiki/Evolutionary_database_design) based methodology including DB versioning, regression testing ([PostgreSQL extensions](https://www.postgresql.org/docs/current/extend-extensions.html)), CI (GitLab).
* Lector of [p2d2](https://p2d2.cz/rocnik-2020/skoleni) [workshop](https://gitlab.com/jurafejfar/edd_workshop/-/wikis/home).

**Administration** of several **Linux DB servers** used in NFI:

* updates, backups, maintenance of servers,
* monitoring, DB performance tuning, query optimization.

**Projects**

2016 - 
:	*Monitoring the state and evolution of Czech forest ecosystems*. Data processing, estimators implementation and testing.

2022 - 
:	*[Pathfinder](https://pathfinder-heu.eu/)* -- Towards an Integrated Consistent European LULUCF Monitoring and Policy Pathway Assessment Framework. Data processing.

2015 - 2019
:	*DIABOLO* -- Distributed, Integrated and Harmonised Forest Information for Bioeconomy Outlooks, implementation of estimation platform.

2015 - 2016
:	*NFI Exchange* -- Exchange of knowledge used in NFI between Czech FMI and Swiss WSL. Workshop, comparison of estimators, simulations, documentation.

2013 - 2016
: *Czech NFI2* Second cycle of Czech National Forest Inventory. Data processing, estimators implementation and testing.

###	International Remote Sensing consultant
**Mar 2016 – Dec 2016 •
[Food and Agriculture Organization (FAO) of the United Nations](http://www.fao.org/about/en/) • Tashkent, UZB**

[FAO](http://www.fao.org/about/en/) project [TCP/UZB/3503](http://www.fao.org/publications/card/en/c/CA5926EN/).
Uzbekistan-wide land cover classification using remote-sensing data and open-source SW:
Linux,
PostgreSQL + PostGIS,
[Orfeo-toolbox](https://www.orfeo-toolbox.org/),
Scikit-learn.

### Research assistant
**Dec 2011 – Aug 2015 •
[Mendel university in Brno](https://mendelu.cz/en/) •
Brno, CZ**

* Data analysis (sound recordings, forestry geo-data, remote sensing images).
* Teaching GIS, Machine Learning (neural networks).
* [researchgate.net/profile/Jiri-Fejfar](https://www.researchgate.net/profile/Jiri-Fejfar), [google scholar](https://scholar.google.com/citations?user=yllcTFYAAAAJ&hl=en)

Education
---------

2008-2011
:   **Ph.D., Systems engineering and Information Systems** – Mendel university in Brno, Faculty of Business and Economics, [Department of Informatics](http://informatika.mendelu.cz/en/article/department-of-informatics)

    Thesis title: *Application of Modern Methods for Sound Data Classification.* ([pdf](https://gitlab.com/jurafejfar/resume/-/raw/master/attachments/disertace_fejfar.pdf)).

2001-2007
:   **Ing., Engineering Computer Science and Automation** – Brno University of Technology, Faculty of mechanical engineering, [Institute of Automation and Computer Science](https://uai.fme.vutbr.cz/)

	Thesis title: *Company Information System Implementation.* ([pdf](https://gitlab.com/jurafejfar/resume/-/raw/master/attachments/DP_Fejfar_el.pdf)).
	
Training and certificates
-------------------------

2019
:	**CAE, CEFR Level C1** – Cambridge Assessment English
	([pdf](https://gitlab.com/jurafejfar/resume/-/raw/master/attachments/CAE_Fejfar.pdf))

2015
:	**[Statistical learning online course](
	https://lagunita.stanford.edu/courses/HumanitiesScience/StatLearning/Winter2014/about)
	** – Stanford Online
	([pdf](https://gitlab.com/jurafejfar/resume/-/raw/master/attachments/Statlearning_2015.pdf))

2013
:	**[Machine learning online course](
	https://www.coursera.org/learn/machine-learning
	)** – Coursera.org
	[verify](https://verify.lagunita.stanford.edu/SOA/6ebccee41f764d65b5ae90cb3eaf9ef1/)
	([pdf](https://gitlab.com/jurafejfar/resume/-/raw/master/attachments/MachineLearning_2013.pdf))

Technical Experience
--------------------

### Programming, SW design, version control, testing, documentation

* Focused on open-source
* Programming in SQL, Python, C
* Implementation of algorithms: matrix algebra / vectorization (Numpy)
* Object-oriented SW design and implementation
* Version control (Git)
* Project management (issues, milestones), GitLab workflow
* Unit / regressin testing
* GitLab CI/CD pipelines
* Documentation (markdown, Pandoc, \LaTeX)

### Databases (PostgreSQL)

* Relational Database design (ERD)
* Development methodologies ([Evolutionary Database Design](https://en.wikipedia.org/wiki/Evolutionary_database_design))
* Execution plans -- understanding, optimization
* Version control and regression testing using [PostgreSQL extensions](https://www.postgresql.org/docs/current/extend-extensions.html) framework

### Data analysis

* Data handling (Numpy, Scipy, Pandas)
* Visualization (Pandas, Matplotlib)
* Machine learning (scikit-learn) and statistics
* Jupyter notebooks

### Infrastructure

* Virtualization concepts, containers (docker, docker-compose)
* Provisioning (Ansible basics)
* Using Cloud Computing Services (AWS basics)

### GIS & Remote sensing

* PostGIS (vector and raster data)
* OrfeoToolBox

Communication and interpersonal skills
--------------------------------------

Emphasizing transparency and empathy.
Problem isolation and solving abilities.
Deconstruction of complicated problems.
Communication on mailing-lists, issue trackers.

Languages:

* Czech: Mother tongue
* English: proficient user (CAE, CEFR Level C1 [certified](https://gitlab.com/jurafejfar/resume/-/raw/master/attachments/CAE_Fejfar.pdf)))

Hobbies and interests
---------------------

**Family** -- Married, 2 kids.
**Music** -- playing violin / viola / singing, composition, [songbook](https://jurafejfar.gitlab.io/zezulci/zpevnik_zezulci.pdf) of Moravian Walachia songs.
**House** -- reconstriction, improvements.
**Sports** -- off-road running, mountain bike, cross-country skiing, longboard riding.
**Technology** -- functional programming in Haskell ([HSoM](https://www.euterpea.com/haskell-school-of-music/)]), DevOps / open-source culture reading.
